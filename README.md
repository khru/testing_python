# About me...
Javier Martinez Alcantara, Javi.

[LinkedIn](https://www.linkedin.com/in/javier-martinez-alcantara/)

[Some info](https://jmaralc.github.io/)

# 🤠 Testing in python: the penitent man will pass...

This is the first repo of a series of repos related to three key topics in the life of a developer. I took the chance to honor the hero of my childhood, Indiana Jones, naming them after the three trials he passed in the movie *the Last Crusade*...not a random choice but rather the idea that developing is an adventure in itself with dangers, adventures and the promise of excitement.

But let me introduce you these key points:
- Testing [15 April 2021]
- Design patterns [14 June 2021]
- Architecture [15 September 2021]

These three are trials that we have in our daily life and there are tons of bibliography and documentation out there. What I pretend here is to treat these topics in an elegant, applied and enjoyable way...and as rigorous as possible.

My wish is that you enjoy and take some learnings home. The adventure could start just by cloning this repo.

⚠️ **IMPORTANT**⚠️
Review all the branches and try to follow the slides to understand the flow of the session.

# Motivation

Testing is not a new thing...and still is a THING. The main motivation of this repo and session is the experience of not seeing much testing around me in my professional experiences.

In many places testing is disregarded, in others is incorporated to the development cycle at late stages of the project, some practice TDD, some others look for coverage and some fake the coverage. 

This session/repo is an open space to treat the topic....Is it even possible coding without testing?

Our focus will be in Unit testing, tools and general ideas.
 
# [Session slides](https://docs.google.com/presentation/d/1zlNP8TFOjFeZ0RgMQ_FsHs8OOA2H4p2TkvWjR0-IKGA/edit?usp=sharing)

# Setup

For this talk you will need python 3.9 and pip installed in your computer. We will use pipenv for installing the packages and keeping the virtual environment. So I recomend you to install it with pip:

```shell
pip install --user --upgrade pipenv
```
Further information about installation [here](https://pipenv-fork.readthedocs.io/en/latest/install.html#installing-pipenv)

# Global Goal

- Spread the knowledge of python
- Promote the use of FastAPI
- Promote the adoption of good practices and technologies

# Structure of the repository/project

The repository presents different branches, divided in steps. The higher the step the more complex will be the code and more functionality will provide. We can summarize the branches as:

- **Master** → Mainly readme

- **Pre-assignment** → Initial explanations about testing tools in python

- **Assignment 1**: *We start a project from scratch!* → Coding and testing session using a simple requirement.

- **Assignment 2**: *We have to deal with an already ongoing project* → Coding session where we will review common traps and pitfalls when arriving to a project without tests (yep, this still a reallity).

- **Assignment 3**: *We have to deal with a more complex project* → a section where we will talk about Mocking and patching.


# How to run it
First set in place pipenv with:

```shell
pipenv install
```

Within the `Pipfile` you will find a section called scripts. In that section you will find a shortcut for calling the needed command to perform an action. So for instance, image that you have a section like:

```toml
[scripts]
start="python ./preassignment/src/example.py"
unittest="python -m unittest -v"
pytest="pytest --cov=preassignment/src --cov-branch --cov-report=term-missing"
pytest_report="pytest --cov=preassignment/src --cov-report html --cov-branch"
```

This would mean that from the shell you just have to run:

```shell
pipenv run pytest_report
```
To run the associated command and get an execution of pytest and a report in HTML. 


# Special thanks to..
For providing an excelent testing framework:
https://docs.pytest.org/en/stable/

For providing 862 plugins (awesome) community!)
https://docs.pytest.org/en/latest/reference/plugin_list.html

For the opportunity to share it with you:
https://codurance.com/

For the nice framework:
https://fastapi.tiangolo.com/

For the best practices and contribution to the community:
https://www.cosmicpython.com/